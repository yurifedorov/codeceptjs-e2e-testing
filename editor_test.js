Feature("Netflix");

Scenario('Show Netflix main page', ({ I }) => {
  I.amOnPage("/");
  I.seeElement(".our-story-card-subtitle", 'Watch anywhere. Cancel anytime.');
})
