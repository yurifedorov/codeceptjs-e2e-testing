let config = {
  tests: "../*_test.js",
  output: "../output",
  helpers: {
    WebDriver: {
      // url: "http://localhost:4200/",
      url: "https://www.netflix.com/",
      browser: "chrome",
      windowSize: "1920x1080",
    },
    REST: {},
  },
  include: {
    I: "../steps_file.js",
  },
  bootstrap: null,
  mocha: {},
  name: "my-auto-e2e-tests",
};

exports.config = config;
